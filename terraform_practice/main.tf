module "my_vpc" {
  source = "./modules/vpc"
  cidr_vpc = var.cidr_vpc_main
  cidr_private = var.cidr_private
  zone_private = var.zone_private
  cidr_public = var.cidr_public
  zone_public = var.zone_public
}

module "myec2" {
  source = "./modules/ec2"
  ami =  var.ami 
  instance_type = var.instance_type
  key_name = var.key_name
  private_subnet_id = module.my_vpc.private_subnet_id
  sg_id = [module.my_vpc.sg_id]
  public_subnet_id = module.my_vpc.public_subnet_id
}


module "autoscalling" {
  source = "./modules/autoscalling"
  instance_type = var.instance_type
  subnet_lists = [module.my_vpc.private_subnet_id, module.my_vpc.public_subnet_id]
  ami = var.ami
  sg_id = [module.my_vpc.sg_id]
  key_name = var.key_name
  availability_zones = [var.zone_private, var.zone_public]
  max_size = var.max_size
  min_size = var.min_size
  desired_capacity = var.desired_capacity
  policy_name = "increase_group_policy"
  alarm_name = "upper_limit_alarm"
}