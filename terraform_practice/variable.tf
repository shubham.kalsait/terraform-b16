variable "cidr_vpc_main" {
  default = "10.10.0.0/16"
}

variable "zone_private" {
  default = "us-east-1a"
}

variable "cidr_private" {
  default = "10.10.0.0/20"
}

variable "cidr_public" {
  default = "10.10.16.0/20"
}

variable "zone_public" {
  default = "us-east-1b"
}

variable "ami" {
  default = "ami-0022f774911c1d690"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "shubham_nv"
}

variable "max_size" {
  type = number
  default = 3
}

variable "min_size" {
  type = number
  default = 1
}

variable "desired_capacity" {
  type = number
  default = 1
}