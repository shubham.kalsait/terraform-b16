output "instance-id" {
  value = aws_instance.app-server.id
  description = "The instance id"
}
