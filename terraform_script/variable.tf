variable "region" {
  default = "us-east-1"
}

variable "ami" {
  default = "ami-0022f774911c1d690"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "security_groups" {
  type = list(string)
  default =  ["default"]
}

variable "key_name" {
  default = "aws-key"
}
